package designPattern.behaviorPattern.visitorPattern;

public interface ShoppingItem {
	double accept(ShoppingCartVisitor visitor);
}
