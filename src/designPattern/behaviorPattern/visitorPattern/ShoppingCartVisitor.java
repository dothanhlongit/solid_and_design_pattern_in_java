package designPattern.behaviorPattern.visitorPattern;

public interface ShoppingCartVisitor {
	double visit(Chair chair);

	double visit(Table table);
}
