package designPattern.behaviorPattern.visitorPattern;


import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		List<ShoppingItem> items = new ArrayList<>();
		items.add(new Table("desk", 20));
		items.add(new Chair("chair1", 10));
		items.add(new Chair("chair2", 15));

		double sum = 0;
		ShoppingCartVisitor shoppingCart = new ShoppingCart();
		for (ShoppingItem item: items) {
			sum += item.accept(shoppingCart);
		}

		System.out.println(sum);
	}
}
