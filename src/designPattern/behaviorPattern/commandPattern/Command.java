package designPattern.behaviorPattern.commandPattern;

public interface Command {
	void execute();
}
