package designPattern.behaviorPattern.commandPattern;

//receiver
public class Light {
	public void turnOn() {
		System.out.println("light is on...");
	}

	public void turnOff() {
		System.out.println("light is off...");
	}
}
