package designPattern.behaviorPattern.commandPattern;

public class Main {
	public static void main(String[] args) {
		Light light = new Light();
		Command offCommand = new TurnOffCommand(light);
		Command onCommand = new TurnOnCommand(light);

		Switcher switcher = new Switcher();
		switcher.addCommand(offCommand);
		switcher.addCommand(onCommand);
		switcher.execute();
	}
}
