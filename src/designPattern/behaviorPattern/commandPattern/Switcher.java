package designPattern.behaviorPattern.commandPattern;

import java.util.ArrayList;
import java.util.List;

//invoker
public class Switcher {
	private List<Command> commands = new ArrayList<>();

	public void addCommand(Command command) {
		this.commands.add(command);
	}

	public void execute() {
		commands.forEach(Command::execute);
	}
}
