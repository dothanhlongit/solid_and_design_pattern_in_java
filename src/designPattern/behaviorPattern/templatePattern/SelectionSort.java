package designPattern.behaviorPattern.templatePattern;

public class SelectionSort extends Algorithm {

	public SelectionSort(int[] nums) {
		super(nums);
	}

	@Override
	protected void sorting() {
		for(int i= 0; i < this.nums.length; i++) {
			int index = i;
			for (int j= i +1; j < this.nums.length; j++) {
				if (nums[j] < nums[index]) {
					index = j;
				}
			}

			swap(i, index);
		}
	}

	@Override
	protected void showResult() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.nums.length; i++) {
			sb.append(this.nums[i]).append(" <= ");
		}

		System.out.println(sb);
	}
}
