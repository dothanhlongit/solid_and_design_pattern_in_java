package designPattern.behaviorPattern.templatePattern;

public class Main {
	public static void main(String[] args) {
		Algorithm algorithm = new SelectionSort(new int[]{1, 5, 4, 9, 2});
		algorithm.sort();
	}
}
