package designPattern.behaviorPattern.templatePattern;

public class BubbleSort extends Algorithm {

	public BubbleSort(int[] nums) {
		super(nums);
	}

	@Override
	protected void sorting() {
		for (int j = this.nums.length - 1; j >= 0; j--) {
			for (int i = 0; i < j; i++) {
				if (nums[i] > nums[j]) {
					this.swap(i, j);
				}
			}
		}
	}

	@Override
	protected void showResult() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < this.nums.length; i++) {
			sb.append(this.nums[i]).append(" <= ");
		}

		System.out.println(sb);
	}
}
