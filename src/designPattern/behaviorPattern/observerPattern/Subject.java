package designPattern.behaviorPattern.observerPattern;

public interface Subject {
	void addObserver(Observer observer);
	void removeObserver(Observer observer);
	void notifyAllObserver();
}
