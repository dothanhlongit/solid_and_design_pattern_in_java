package designPattern.behaviorPattern.observerPattern;

import designPattern.behaviorPattern.strategyPattern.Addition;
import designPattern.behaviorPattern.strategyPattern.OperationManager;

public class Main {
	public static void main(String[] args) {
		WeatherStation weatherStation = new WeatherStation();
		Observer weatherObserver = new WeatherObserver(weatherStation);

		weatherStation.setPressure(20);
		weatherStation.setHumidity(30);

	}
}
