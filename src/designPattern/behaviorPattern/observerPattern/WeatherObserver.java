package designPattern.behaviorPattern.observerPattern;

public class WeatherObserver implements Observer {
	private int pressure;
	private int temperature;
	private int humidity;
	private Subject subject;

	public WeatherObserver(Subject subject) {
		this.subject = subject;
		this.subject.addObserver(this);
	}

	@Override
	public void update(int pressure, int temperature, int humidity) {
		this.pressure = pressure;
		this.temperature = temperature;
		this.humidity = humidity;

		this.toString();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("pressure: ").append(pressure);
		sb.append("temperature: ").append(temperature);
		sb.append("humidity: ").append(humidity);
		System.out.println(sb);
		return sb.toString();
	}
}
