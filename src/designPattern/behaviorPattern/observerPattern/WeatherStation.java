package designPattern.behaviorPattern.observerPattern;

import java.util.LinkedList;
import java.util.List;

public class WeatherStation implements Subject {
	private int pressure;
	private int temperature;
	private int humidity;
	private List<Observer> observerList = new LinkedList<>();

	@Override
	public void addObserver(Observer observer) {
		observerList.add(observer);
	}

	@Override
	public void removeObserver(Observer observer) {
		observerList.remove(observer);
	}

	@Override
	public void notifyAllObserver() {
		observerList.forEach(o -> o.update(this.pressure, this.temperature, this.humidity));
	}

	public void setPressure(int pressure) {
		this.pressure = pressure;
		this.notifyAllObserver();
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
		this.notifyAllObserver();
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
		this.notifyAllObserver();
	}
}
