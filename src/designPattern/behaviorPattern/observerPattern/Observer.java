package designPattern.behaviorPattern.observerPattern;

public interface Observer {
	void update(int pressure, int temperature, int humidity);
}
