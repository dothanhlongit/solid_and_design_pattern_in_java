package designPattern.behaviorPattern.strategyPattern;

public class Main {
	public static void main(String[] args) {
		new OperationManager(new Addition()).execute(1, 2);
	}
}
