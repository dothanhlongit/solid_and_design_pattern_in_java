package designPattern.behaviorPattern.strategyPattern;

public class OperationManager {
	private MathematicalOperation mathematicalOperation;

	public OperationManager(MathematicalOperation operation) {
		this.mathematicalOperation = operation;
	}

	public void execute(int num1, int num2) {
		this.mathematicalOperation.execute(num1, num2);
	}
}
