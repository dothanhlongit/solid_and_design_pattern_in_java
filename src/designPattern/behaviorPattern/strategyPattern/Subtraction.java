package designPattern.behaviorPattern.strategyPattern;

public class Subtraction implements MathematicalOperation {

	@Override
	public void execute(int num1, int num2) {
		System.out.println(num1 - num2);
	}
}
