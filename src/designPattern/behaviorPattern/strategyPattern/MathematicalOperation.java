package designPattern.behaviorPattern.strategyPattern;

public interface MathematicalOperation {
	void execute(int num1, int num2);
}
