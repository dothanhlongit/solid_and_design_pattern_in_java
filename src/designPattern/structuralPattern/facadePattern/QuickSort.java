package designPattern.structuralPattern.facadePattern;

public class QuickSort implements Algorithm {
	@Override
	public void sort() {
		System.out.println("using quick sort");
	}
}
