package designPattern.structuralPattern.facadePattern;

public class BubbleSort implements Algorithm {
	@Override
	public void sort() {
		System.out.println("using bubble sort");
	}
}
