package designPattern.structuralPattern.facadePattern;

public class MergeSort implements Algorithm {
	@Override
	public void sort() {
		System.out.println("using merge sort");
	}
}
