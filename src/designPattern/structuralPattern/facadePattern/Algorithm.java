package designPattern.structuralPattern.facadePattern;

public interface Algorithm {
	void sort();
}
