package designPattern.structuralPattern.facadePattern;


public class Main {
	public static void main(String[] args) {
		SortingManager sortingManager = new SortingManager();
		sortingManager.doBubbleSort();
		sortingManager.doMergeSort();
		sortingManager.doQuickSort();
	}
}
