package designPattern.structuralPattern.flyweightPattern;

import java.util.Random;

public class FlyweightSimulator {
	private Random random;
	private String[] colors = {"RED", "GREEN", "BLUE"};
	private ShapeFactory shapeFactory;

	public FlyweightSimulator(){
		this.random = new Random();
		this.shapeFactory = new ShapeFactory();
	}

	public void run() {
		for (int i = 0; i < 30; i ++) {
			Rectangle rectangle = (Rectangle) shapeFactory.getShape(getRandomColor());
			rectangle.setX(getRandomCoordinate());
			rectangle.setY(getRandomCoordinate());
			rectangle.draw();
		}
	}

	private int getRandomCoordinate() {
		return random.nextInt(50);
	}

	private String getRandomColor() {
		return colors[random.nextInt(colors.length)];
	}
}
