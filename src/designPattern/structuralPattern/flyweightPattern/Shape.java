package designPattern.structuralPattern.flyweightPattern;

public interface Shape {
	void draw();
}
