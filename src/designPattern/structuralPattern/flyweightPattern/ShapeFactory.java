package designPattern.structuralPattern.flyweightPattern;

import java.util.HashMap;
import java.util.Map;

public class ShapeFactory {

	private Map<String, Shape> shapeMap;

	public ShapeFactory() {
		shapeMap = new HashMap<>();
	}

	public Shape getShape(String color) {
		if (shapeMap.containsKey(color)) {
			return shapeMap.get(color);
		}
		System.out.println("creating new rectangle object");
		shapeMap.put(color, new Rectangle(color));
		return shapeMap.get(color);
	}
}
