package designPattern.structuralPattern.decoratorPattern;

public class Milk extends BeverageDecorator {
	private Beverage beverage;

	public Milk(Beverage beverage) {
		super(beverage);
		this.beverage = beverage;
	}

	@Override
	public int getCost() {
		return super.getCost() + 3;
	}

	@Override
	public String getDescription() {
		return super.getDescription() + " milk";
	}
}
