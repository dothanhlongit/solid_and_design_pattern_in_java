package designPattern.structuralPattern.decoratorPattern;

public class Sugar extends BeverageDecorator {
	private Beverage beverage;

	public Sugar(Beverage beverage) {
		super(beverage);
		this.beverage = beverage;
	}

	@Override
	public int getCost() {
		return super.getCost() + 1;
	}

	@Override
	public String getDescription() {
		return super.getDescription() + " sugar";
	}
}
