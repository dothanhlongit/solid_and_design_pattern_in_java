package designPattern.structuralPattern.decoratorPattern;

public interface Beverage {
	int getCost();

	String getDescription();
}
