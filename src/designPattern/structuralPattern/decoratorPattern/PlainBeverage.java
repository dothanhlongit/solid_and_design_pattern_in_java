package designPattern.structuralPattern.decoratorPattern;

public class PlainBeverage implements Beverage {
//	private Beverage beverage;

//	public PlainBeverage(Beverage beverage) {
//		this.beverage = beverage;
//	}

	@Override
	public int getCost() {
		return 5;
	}

	@Override
	public String getDescription() {
		return "plain beverage";
	}
}
