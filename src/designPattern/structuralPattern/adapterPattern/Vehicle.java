package designPattern.structuralPattern.adapterPattern;

public interface Vehicle {
	void accelerate();
}
