package designPattern.structuralPattern.adapterPattern;

public class Bicycle {
	public void go() {
		System.out.println("using bicycle...");
	}
}
