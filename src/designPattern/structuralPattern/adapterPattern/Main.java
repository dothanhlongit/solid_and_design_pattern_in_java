package designPattern.structuralPattern.adapterPattern;


public class Main {
	public static void main(String[] args) {
		Vehicle bus = new Bus();
		bus.accelerate();

		Vehicle bicycle = new BicycleAdapter(new Bicycle());
		bicycle.accelerate();
	}
}
