package designPattern.creationalPattern.factoryPattern;

public class AnimalFactory {
	public static Animal getAnimal(ANIMAL_TYPE animalType) {
		if (animalType == null) {
			throw new IllegalArgumentException();
		}

		switch (animalType) {
			case DOG:
				return new Dog();
			case CAT:
				return new Cat();
			case TIGER:
				return new Tiger();
			case LION:
				return new Lion();
			default:
				return null;
		}
	}
}
