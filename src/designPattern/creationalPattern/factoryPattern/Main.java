package designPattern.creationalPattern.factoryPattern;

public class Main {
	public static void main(String[] args) {
		Animal animal = AnimalFactory.getAnimal(ANIMAL_TYPE.DOG);
	}
}
