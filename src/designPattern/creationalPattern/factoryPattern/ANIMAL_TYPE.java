package designPattern.creationalPattern.factoryPattern;

public enum ANIMAL_TYPE {
	DOG, CAT, TIGER, LION
}
