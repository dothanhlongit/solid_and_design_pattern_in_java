package designPattern.creationalPattern.singletonPattern;

public class Main {
	public static void main(String[] args) {
		DatabaseConnector.INSTANCE.connect();
		DatabaseConnector.getInstance().disConnect();
	}
}
