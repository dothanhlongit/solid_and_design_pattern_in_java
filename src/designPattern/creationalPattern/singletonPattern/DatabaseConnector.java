package designPattern.creationalPattern.singletonPattern;

public class DatabaseConnector {
	public static DatabaseConnector INSTANCE = new DatabaseConnector();

	// can't init an object
	private DatabaseConnector() {
	}

	public static DatabaseConnector getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new DatabaseConnector();
		}

		return INSTANCE;
	}

	public void connect() {
		System.out.println("connecting to the database...");
	}

	public void disConnect() {
		System.out.println("disConnecting to the database");
	}
}
