package designPattern.creationalPattern.prototypePattern;

public class Rectangle extends Shape {

	public Rectangle(int width, int height) {
		super(width, height);
	}

	@Override
	public void draw() {
		System.out.println("draw rectangle");
	}

	@Override
	public Shape cloneObject() {
		return new Rectangle(width, height);
	}
}
