package designPattern.creationalPattern.prototypePattern;

public class Square extends Shape {

	public Square(int width, int height) {
		super(width, height);
	}

	@Override
	public void draw() {
		System.out.println("draw square");
	}

	@Override
	public Shape cloneObject() {
		return new Square(width, height);
	}
}
