package designPattern.creationalPattern.builderPattern;


public class Main {
	public static void main(String[] args) {
		Person person = new Person.Builder("Long").setEmail("dothanhlongit@gmail.com").build();
		System.out.println(person.toString());
	}
}
