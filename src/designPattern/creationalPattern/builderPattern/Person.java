package designPattern.creationalPattern.builderPattern;

public class Person {
	private String name;
	private String email;
	private String address;
	private int age;

	public Person(Builder builder) {
		this.name = builder.name;
		this.email = builder.email;
		this.address = builder.address;
		this.age = builder.age;
	}

	public String toString() {
		return this.name + "-" + this.email + "-" + this.address + "-" + this.age;
	}

	public static class Builder {
		private final String name;
		private String email;
		private String address;
		private int age;

		public Builder(String name) {
			this.name = name;
		}

		public Builder setEmail(String email) {
			this.email = email;
			return this;
		}

		public Builder setAddress(String address) {
			this.address = address;
			return this;
		}

		public Builder setAge(int age) {
			this.age = age;
			return this;
		}

		public Person build() {
			return new Person(this);
		}
	}
}
