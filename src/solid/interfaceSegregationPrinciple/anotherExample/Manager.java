package solid.interfaceSegregationPrinciple.anotherExample;

public class Manager implements IManager{
	@Override
	public void salary() {

	}

	@Override
	public void addBonus() {

	}

	@Override
	public void hire() {

	}

	@Override
	public void train() {

	}
}
