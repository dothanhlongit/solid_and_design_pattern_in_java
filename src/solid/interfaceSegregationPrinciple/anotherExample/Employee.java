package solid.interfaceSegregationPrinciple.anotherExample;

public interface Employee {
	// CEO + managers + workers
	void salary();
}
