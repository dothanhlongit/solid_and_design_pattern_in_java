package solid.interfaceSegregationPrinciple.anotherExample;

public interface ManagementLevel {

	// CEO + managers
	public void addBonus();
}
