package solid.interfaceSegregationPrinciple.anotherExample;

public interface IManager extends Employee, ManagementLevel {
	// managers
	public void hire();

	public void train();

}
