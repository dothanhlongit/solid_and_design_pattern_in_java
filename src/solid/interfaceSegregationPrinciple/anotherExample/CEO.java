package solid.interfaceSegregationPrinciple.anotherExample;

public class CEO implements ICEO{
	@Override
	public void salary() {

	}

	@Override
	public void makeDecisions() {

	}

	@Override
	public void addStocks() {

	}


	@Override
	public void addBonus() {

	}
}
