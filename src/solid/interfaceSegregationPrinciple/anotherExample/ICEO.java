package solid.interfaceSegregationPrinciple.anotherExample;

public interface ICEO extends Employee, ManagementLevel {
	// CEO
	public void makeDecisions();

	public void addStocks();
}
