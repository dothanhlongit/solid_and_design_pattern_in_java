package solid.interfaceSegregationPrinciple;

public class BalancedTree implements RotationTree {
	@Override
	public void insert() {
		System.out.println("insert method");
	}

	@Override
	public void delete() {
		System.out.println("delete method");
	}

	@Override
	public void traverse() {
		System.out.println("traverse the tree");
	}

	@Override
	public void leftRotation() {
		System.out.println("left rotation");
	}

	@Override
	public void rightRotation() {
		System.out.println("right rotation");
	}
}
