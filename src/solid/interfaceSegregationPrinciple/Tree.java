package solid.interfaceSegregationPrinciple;

public interface Tree {
	void insert();
	void delete();
	void traverse();
}
