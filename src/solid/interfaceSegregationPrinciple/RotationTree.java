package solid.interfaceSegregationPrinciple;

public interface RotationTree extends Tree {
	void leftRotation();

	void rightRotation();
}
