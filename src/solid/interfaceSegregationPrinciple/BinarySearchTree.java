package solid.interfaceSegregationPrinciple;

public class BinarySearchTree implements Tree {
	@Override
	public void insert() {
		System.out.println("insert method");
	}

	@Override
	public void delete() {
		System.out.println("delete method");
	}

	@Override
	public void traverse() {
		System.out.println("traverse the tree");
	}
}
