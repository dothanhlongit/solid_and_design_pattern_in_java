package solid.singleResponsibility;

public class ViolationChecker {
    public static boolean isValid(NumerPair pair) {
        try{
            Integer.parseInt(pair.getFirst());
            Integer.parseInt(pair.getSecond());
        } catch (NumberFormatException e){
            return false;
        }

        return true;
    }

    public static boolean isNumberAndSmallerThan10AndGreaterThan0(String input) {
        try{
            int intInput = Integer.parseInt(input);
            return 0 < intInput &&intInput <= 10;
        } catch (NumberFormatException e){
            return false;
        }
    }
}
