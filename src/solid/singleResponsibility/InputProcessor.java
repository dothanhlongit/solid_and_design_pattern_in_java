package solid.singleResponsibility;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputProcessor {
    public static final int THRESHOLD = 5;

    public static NumerPair process() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("enter the first number");
        String first = scanner.next();

        System.out.println("enter the second number");
        String second = scanner.next();

        return new NumerPair(first, second);
    }

    public static List<Integer> processArray() {
        Scanner scanner = new Scanner(System.in);

        List<Integer> nums = new ArrayList<>();

        System.out.println("Enter 5 valid integers in the range [0, 10]");

        while(nums.size() < THRESHOLD) {

            String s = scanner.nextLine();

            if(!ViolationChecker.isNumberAndSmallerThan10AndGreaterThan0(s)){
                System.out.println("invalid input: must be number between 1 and 10");
            }
            nums.add(Integer.parseInt(s));
        }

        scanner.close();

        return nums;
    }
}
