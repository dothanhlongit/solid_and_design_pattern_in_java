package solid.singleResponsibility.anotherExample;

import java.util.List;

public class Printer {
    public static void print(List<Integer> nums){
        StringBuilder stringBuilder = new StringBuilder();
        for(int num : nums){
            stringBuilder.append(num).append("  ");
        }

        System.out.println(stringBuilder);
    }
}
