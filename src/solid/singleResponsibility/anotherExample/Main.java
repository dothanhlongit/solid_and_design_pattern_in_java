package solid.singleResponsibility.anotherExample;

import solid.singleResponsibility.InputProcessor;
import solid.singleResponsibility.NumerPair;
import solid.singleResponsibility.Operation;
import solid.singleResponsibility.ViolationChecker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Welcome to the Application!");
        List<Integer> nums = InputProcessor.processArray();
        Collections.sort(nums);
        Printer.print(nums);
    }
}
