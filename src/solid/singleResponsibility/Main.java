package solid.singleResponsibility;

public class Main {
    public static void main(String[] args) {
        NumerPair pair = InputProcessor.process();
        if (!ViolationChecker.isValid(pair)) {
            System.out.println("one input invalid");
            return;
        }
        int result = Operation.add(Integer.parseInt(pair.getFirst()), Integer.parseInt(pair.getSecond()));
        System.out.println("the result of add operation is: " + result);
        System.out.println("closing");
    }
}
