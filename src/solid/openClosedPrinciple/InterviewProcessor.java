package solid.openClosedPrinciple;


public class InterviewProcessor {
    public static void process(InterviewQuestions interviewQuestions) {
        interviewQuestions.execute();
    }
}
