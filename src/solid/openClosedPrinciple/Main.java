package solid.openClosedPrinciple;

public class Main {
    public static void main(String[] args) {
        InterviewProcessor.process(new AlgorithmInterviewQuestions());
    }
}
