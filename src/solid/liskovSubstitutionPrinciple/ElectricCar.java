package solid.liskovSubstitutionPrinciple;

public class ElectricCar extends Vehicle {

	public ElectricCar(String type, int age) {
		super(type, age);
	}

	@Override
	protected void speedUp() {
		System.out.println("the electric car is speeding up....");
	}
	
	@Override
	protected void slowDown() {
		System.out.println("the electric car is slowing down....");
	}

	@Override
	protected void fuel() {
		System.out.println("the electric car is charging");
	}
}
