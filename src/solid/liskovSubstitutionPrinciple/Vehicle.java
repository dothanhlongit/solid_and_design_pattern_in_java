package solid.liskovSubstitutionPrinciple;

public abstract class Vehicle {
	protected String type;
	protected int age;

	public Vehicle(String type, int age) {
		this.type = type;
		this.age = age;
	}

	protected void speedUp() {
		System.out.println("the vehicle is speeding up....");
	}

	protected void slowDown() {
		System.out.println("the vehicle is slowing down....");
	}

	protected abstract void fuel();
}
