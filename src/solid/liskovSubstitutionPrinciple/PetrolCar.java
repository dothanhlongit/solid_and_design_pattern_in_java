package solid.liskovSubstitutionPrinciple;

public class PetrolCar extends Vehicle {

	public PetrolCar(String type, int age) {
		super(type, age);
	}

	@Override
	protected void speedUp() {
		System.out.println("the car is speeding up....");
	}

	@Override
	protected void slowDown() {
		System.out.println("the car is slowing down....");
	}

	@Override
	protected void fuel() {
		System.out.println("the car is refueling");
	}
}
