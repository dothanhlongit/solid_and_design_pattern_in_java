package solid.liskovSubstitutionPrinciple;

public class Main {
    public static void main(String[] args) {
        Vehicle v = new ElectricCar("Tesla", 6);
        v.speedUp();
        v.slowDown();
        v.fuel();
    }
}
