package solid.dependencyInversion;

public interface Database {
	public void connect();
	public void disConnect();
}
