package solid.dependencyInversion;

//high level module
public class DatabaseController {
	private Database database;

	public DatabaseController(Database database) {
		this.database = database;
		database.connect();
		database.disConnect();
	}
}
