package solid.dependencyInversion;

public class Main {
	public static void main(String[] args) {
		new DatabaseController(new MySQLDatabase());
		new DatabaseController(new PostgresSQLDatabase());
	}
}
