package solid.dependencyInversion;

//low level module
public class PostgresSQLDatabase implements Database {
	public void connect() {
		System.out.println("connecting Postgres database...");
	}

	public void disConnect() {
		System.out.println("disConnecting Postgres database...");
	}
}
