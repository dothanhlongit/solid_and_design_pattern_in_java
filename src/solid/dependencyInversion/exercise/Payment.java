package solid.dependencyInversion.exercise;

public interface Payment {
	void withdraw();
}
