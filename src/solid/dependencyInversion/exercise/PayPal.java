package solid.dependencyInversion.exercise;

public class PayPal implements Payment {
	@Override
	public void withdraw() {
		System.out.println("withdraw paypal...");
	}
}
