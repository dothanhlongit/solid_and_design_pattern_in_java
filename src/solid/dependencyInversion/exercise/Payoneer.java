package solid.dependencyInversion.exercise;

public class Payoneer implements Payment {
	@Override
	public void withdraw() {
		System.out.println("Payoneer paypal...");
	}
}
