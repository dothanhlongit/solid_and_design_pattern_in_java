package solid.dependencyInversion.exercise;

public class Main {
	public static void main(String[] args) {
		new Udemy(new Payoneer());
		new Udemy(new Skrill());
		new Udemy(new PayPal());
	}
}
