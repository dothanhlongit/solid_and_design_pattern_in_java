package solid.dependencyInversion;

//low level module
public class MySQLDatabase implements Database {
	public void connect() {
		System.out.println("connecting mySQL database...");
	}

	public void disConnect() {
		System.out.println("disConnecting mySQL database...");
	}
}
